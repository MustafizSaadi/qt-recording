#ifndef SCREENCAPTURE_PLUGIN_H
#define SCREENCAPTURE_PLUGIN_H

#include <QQmlExtensionPlugin>

class ScreenCapturePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // SCREENCAPTURE_PLUGIN_H
