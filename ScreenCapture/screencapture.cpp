#include "screencapture.h"
#include <QCoreApplication>
#include <QDir>
#include <QStandardPaths>
#include <QFile>

ScreenCapture::ScreenCapture(QObject *parent)
    : QObject(parent), ffmpegProcess(new QProcess(parent)), runningApplication(new QProcess(parent)), mergeProcess(new QProcess(parent))
{
    runningApplication->start("tasklist", QStringList() << "/v" << "/fi" << "status eq Running" << "/fo" << "csv");
    QObject::connect(runningApplication, &QProcess::readyReadStandardOutput, this, &ScreenCapture::readProcessList);

//    ffmpegProcess->kill();

    connect(mergeProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [=](int exitCode, QProcess::ExitStatus exitStatus){ qDebug() << "Process finished " << exitCode << " " << exitStatus; });

    connect(mergeProcess, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(errorHandle(QProcess::ProcessError)));
}

ScreenCapture::~ScreenCapture()
{
}

bool ScreenCapture::toggleRecording()
{
    qDebug() << "screen capture started " << ffmpegProcess->state();
    if(ffmpegProcess->state() != QProcess::Running)
    {
        QString program = QCoreApplication::applicationDirPath()+"/ffmpeg.exe";
        QStringList arguments;
        QFile outputFile(m_saveLocation);

        if(outputFile.exists())
        {
            if(!outputFile.remove())
                return false;
        }

        arguments
                << "-f"
                << "gdigrab"
                << "-framerate" << m_frameRate
                   //<< "-video_size" << m_resolution
                << "-i" << "desktop"
                << m_saveLocation
                << "-c:v" << "h264_nvenc";
        qDebug() << "Starting " + program;
        ffmpegProcess->start(program, arguments);
    }
    else
    {
        qDebug() << "Stopping";
        ffmpegProcess->write("q", 1);
        ffmpegProcess->closeWriteChannel();
        ffmpegProcess->terminate();
    }
    return true;
}

bool ScreenCapture::mergeRecordings()
{
//    ffmpeg -i file.mkv -itsoffset 3 -i file.mkv -c:a copy -c:v copy -map 0:v:0 -map 1:a:0 out.mkv

//    if(ffmpegProcess->state() == QProcess::Running) {
//        ffmpegProcess->close();
//    }

    QString program = QCoreApplication::applicationDirPath()+"/ffmpeg.exe";
    QStringList arguments;
    QFile outputFileS(m_saveLocation);

    QDir dir(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
    QString subdir = "recording/audio";
    dir.mkpath(subdir);

    QFile outputFileI(dir.path().append("/"+subdir+"/output.mp3"));
    QFile outputFIleF(QCoreApplication::applicationDirPath()+"/final_output.mkv");

    qDebug() << outputFileI.fileName() << " " << outputFileI.size();
    qDebug() << outputFileS.size();

//    if(outputFileS.exists())
//    {
//        if(!outputFile.remove())
//            return false;
//    }

    arguments
            << "-y"
            << "-i"
            << outputFileS.fileName()
            << "-itsoffset" << "1"
               //<< "-video_size" << m_resolution
            << "-i" << outputFileI.fileName()
            << "-c:a" << "copy"
            << "-c:v" << "copy"
            << "-map" << "0:v:0"
            << "-map" << "1:a:0"
            << outputFIleF.fileName()
               ;
    qDebug() << "before merge recording " << mergeProcess->state();
    mergeProcess->start(program, arguments);

    qDebug() << "in merge recording " << mergeProcess->state();

    return true;
}

void ScreenCapture::errorHandle(QProcess::ProcessError err)
{
    qDebug() << "error " << err;
}

void ScreenCapture::readProcessList()
{
    QByteArray stdOut = runningApplication->readAllStandardOutput();
    //qDebug() << stdOut;
    QString totalStdOut = QString::fromStdString(stdOut.toStdString());
    QStringList stdList = totalStdOut.split("\r\n");

    for(int i = 0 ; i < stdList.size(); i++)
    {
        QStringList tmpString = stdList[i].split(',');
        if(tmpString[tmpString.size()-1] != "N/A")
        {
            m_windowTitles.append(tmpString[tmpString.size()-1]);
            qDebug() << m_windowTitles;
        }
    }

}

QString ScreenCapture::getFrameRate()
{
    return m_frameRate;
}

QString ScreenCapture::getResolution()
{
    return m_resolution;
}

QString ScreenCapture::getSaveLocation()
{
    return m_saveLocation;
}

void ScreenCapture::setFrameRate(const QString& frameRate)
{
    if(m_frameRate == frameRate) return;

    m_frameRate = frameRate;

    emit frameRateChanged();

    return;
}

void ScreenCapture::setResolution(const QString &resolution)
{
    if(m_resolution == resolution) return;

    m_resolution = resolution;

    emit resolutionChanged();

    return;
}

void ScreenCapture::setSaveLocation(const QString &saveLocation)
{
    if(m_saveLocation == saveLocation) return;

    m_saveLocation = saveLocation;

    emit saveLocationChanged();

    return;
}

