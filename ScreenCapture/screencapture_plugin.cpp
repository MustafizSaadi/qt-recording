#include "screencapture_plugin.h"

#include "screencapture.h"

#include <qqml.h>

void ScreenCapturePlugin::registerTypes(const char *uri)
{
    // @uri ScreenCapture
    qmlRegisterType<ScreenCapture>(uri, 1, 0, "ScreenCapture");
}

