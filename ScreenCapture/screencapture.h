#ifndef SCREENCAPTURE_H
#define SCREENCAPTURE_H

#include <QObject>
#include <QProcess>
#include <qqml.h>

class ScreenCapture : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ScreenCapture)
    QML_ELEMENT

    Q_PROPERTY(QString resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged)
    Q_PROPERTY(QString frameRate READ getFrameRate WRITE setFrameRate NOTIFY frameRateChanged)
    Q_PROPERTY(QString saveLocation READ getSaveLocation WRITE setSaveLocation NOTIFY saveLocationChanged)
public:
    explicit ScreenCapture(QObject *parent = nullptr);
    ~ScreenCapture() override;

    QString getResolution();
    QString getFrameRate();
    QString getSaveLocation();

    void setResolution(const QString& resolution);
    void setFrameRate(const QString& frameRate);
    void setSaveLocation(const QString& saveLocation);

    Q_INVOKABLE bool toggleRecording();
    Q_INVOKABLE bool mergeRecordings();

public slots:
    void readProcessList();
    void errorHandle(QProcess::ProcessError);

signals:
    void resolutionChanged();
    void frameRateChanged();
    void saveLocationChanged();
private:
    QProcess* ffmpegProcess;
    QProcess* runningApplication;
    QProcess* mergeProcess;

    QString m_resolution;
    QString m_saveLocation;
    QString m_frameRate;
    QList<QString> m_windowTitles;
};

#endif // SCREENCAPTURE_H
