#ifndef AUDIOCAPTURE_H
#define AUDIOCAPTURE_H

#define __STDC_CONSTANT_MACROS

//extern "C" {
//#include "libavdevice/avdevice.h"
//#include "libavcodec/codec.h"
//#include "libavformat/avformat.h"
////#include "./ThirdParty/ffmpeg/include/libavcodec/codec.h"
////#include <libavutil/common.h>
////#include <libavcodec/codec.h>
//}
#include <QDebug>
#include <QProcess>
//#include "AudioBackEnd_ffmpeg_global.h"
#include "qqmlregistration.h"

//AUDIOBACKEND_FFMPEG_EXPORT

class AudioCapture: public QObject
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit AudioCapture(QObject *parent = nullptr);

public slots:
    void started();
    void readyReadStandardOutput();
    Q_INVOKABLE bool toggleRecording();
//    void pause();
    void stopped();
    void errorHandle(QProcess::ProcessError);
//    void exit();

private:
    QProcess *mRecordingProcess;
};

#endif // AUDIOCAPTURE_H
