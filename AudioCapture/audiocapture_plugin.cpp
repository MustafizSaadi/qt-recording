#include "audiocapture_plugin.h"
#include "audiocapture.h"

#include "qqml.h"

void AudioCapturePlugin::registerTypes(const char *uri)
{
    // @uri ScreenCapture
    qmlRegisterType<AudioCapture>(uri, 1, 0, "AudioCapture");
}

