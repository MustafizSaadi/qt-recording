#ifndef AUDIOCAPTURE_PLUGIN_H
#define AUDIOCAPTURE_PLUGIN_H

#include <QQmlExtensionPlugin>

class AudioCapturePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)
public:
//    explicit AudioCapture_Plugin(QObject *parent = nullptr);
    void registerTypes(const char *uri) override;
};

#endif // AUDIOCAPTURE_PLUGIN_H
