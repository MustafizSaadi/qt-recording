#include "audiocapture.h"
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>

AudioCapture::AudioCapture(QObject *parent) : QObject(parent)
{
    //    avcodec_register_all();
    //    avdevice_register_all();
    //    AVFormatContext *fmt_ctx = avformat_alloc_context();
    //    fmt_ctx->iformat = av_find_input_format("dshow");
    ////    fmt_ctx->oformat = new AVOutputFormat;
    //    static struct AVDeviceInfoList* devices_list;
    //    int devices_count = avdevice_list_devices(fmt_ctx, &devices_list);
    //    int devices_count = devices_list->nb_devices;

    //    bool flag = fmt_ctx->oformat || fmt_ctx->iformat;

    //    avdevice_register_all();
    //    const AVInputFormat *iformat = av_find_input_format("dshow");
    //    printf("========Device Info=============\n");
    //    AVDeviceInfoList *device_list = NULL;
    //    AVDictionary* options = NULL;
    //    //av_dict_set(&options, "list_devices", "true", 0);
    //    int result = avdevice_list_input_sources(iformat, NULL, options, &device_list);

    //    if (result < 0)
    //        qDebug() << result;//Returns -40 AVERROR(ENOSYS)
    //    else printf("Devices count:%d\n", result);

    //    AVFormatContext *pFormatCtx = avformat_alloc_context();
    //    AVDictionary* options2 = NULL;
    //    av_dict_set(&options2, "list_devices", "true", 0);
    //    avformat_open_input(&pFormatCtx, NULL, iformat, &options2);
    //    printf("================================\n");

    //    qDebug() << devices_count;

    //    avformat_free_context(fmt_ctx);

    mRecordingProcess = new QProcess(this);

    //    connect(mRecordingProcess, SIGNAL(started()), this, SLOT(started()));

    connect(mRecordingProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(readyReadStandardOutput()));
    //    connect(mRecordingProcess, SIGNAL(finished(int)), this, SLOT(stopped()));
    connect(mRecordingProcess, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(errorHandle(QProcess::ProcessError)));
}

void AudioCapture::started()
{
    mRecordingProcess->setProgram("C:/Users/mustafizur/Documents/AudioRecording/AudioBackEnd_ffmpeg/ThirdParty/ffmpeg/bin/ffmpeg");

    QStringList arguments;

    QDir dir(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
    QString subdir = "recording/audio";
    dir.mkpath(subdir);
    dir.path().append(subdir);

    arguments << "-f" << "dshow" << "-i" << "audio=Microphone (Realtek High Definition Audio)" << "C:/Users/mustafizur/Videos/recording/audio/output5.mp3";

    mRecordingProcess->setArguments(arguments);

    //    mRecordingProcess->kill();
    qDebug() << mRecordingProcess->state();

    mRecordingProcess->start();

    qDebug() << mRecordingProcess->state();
}

bool AudioCapture::toggleRecording()
{
    if(mRecordingProcess->state() != QProcess::Running)
    {
        qDebug() << mRecordingProcess->state();
        QString program = QCoreApplication::applicationDirPath()+"/ffmpeg.exe";
        QStringList arguments;

        QDir dir(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
        QString subdir = "recording/audio";
        dir.mkpath(subdir);

        QFile outputFile(dir.path().append("/"+subdir+"/output.mp3"));

        if(outputFile.exists())
        {
            if(!outputFile.remove())
                return false;
        }

        arguments
                << "-f"
                << "dshow"
                << "-i" << "audio=Microphone (Realtek High Definition Audio)"
                << outputFile.fileName();

//        qDebug() << "Starting " + program;
        mRecordingProcess->start(program, arguments);
        qDebug() << mRecordingProcess->state() << " " << dir.path();
    }
    else
    {
        qDebug() << "Stopping";
        mRecordingProcess->write("q", 1);
//        mRecordingProcess->closeWriteChannel();
        mRecordingProcess->terminate();
        qDebug() << mRecordingProcess->state();
    }
    return true;
}
void AudioCapture::readyReadStandardOutput()
{
    qDebug() << mRecordingProcess->readAllStandardOutput();
}

void AudioCapture::stopped()
{
    //    qDebug(" in stopped");
    mRecordingProcess->write("q");
    mRecordingProcess->closeWriteChannel();
    mRecordingProcess->terminate();
    qDebug() << mRecordingProcess->state();
}

void AudioCapture::errorHandle(QProcess::ProcessError err)
{
    qDebug() << err ;
}
