import QtQuick 2.15
import QtQuick.Window 2.15
import ScreenCapture 1.0
import AudioCapture 1.0
import QtQuick.Controls.Material 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    AudioCapture {id: audioTest}

    ScreenCapture {
        id: screenTest
        frameRate: framerateInput.text
        saveLocation: fileName.text
        resolution: resolution.text
    }

    Column {
        anchors.centerIn: parent

        Row {
            spacing: 10

            Text {
                text: "FrameRate"
                padding: 6
                font.pointSize: 12
            }

            TextField {
                id: framerateInput

                text: "60"
            }
        }

        Row {
            spacing: 10

            Text {
                text: "Output File Name"
                padding: 6
                font.pointSize: 12
            }

            TextField {
                id: fileName
                text: "Output.mp4"
            }
        }

        Row {
            spacing: 10

            Text {
                text: "Resolution"
                padding: 6
                font.pointSize: 12
            }

            TextField {
                id: resolution
                text: "1920x1080"
            }
        }


        Button {
            id: recordButton

            text: "Start Recording"

            onPressed: {
//                audioTest.toggleRecording()
//                screenTest.toggleRecording()
                if(audioTest.toggleRecording() && screenTest.toggleRecording() && recordButton.text === "Start Recording")
                {
                    recordButton.text = "Stop Recording"
                }
                else
                {
                    recordButton.text = "Organizing files ..."
//                    screenTest.mergeRecordings()
                    timer.start()
                    recordButton.text = "Start Recording"
                }
            }
        }
    }

    Timer {
            id: timer
            interval: 3000; running: false;
            onTriggered: screenTest.mergeRecordings()
        }

}
